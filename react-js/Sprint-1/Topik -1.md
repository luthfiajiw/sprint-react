# Prasyarat
- Santri menggunakan Linux sebagai lingkungan kerja
- PHP parser engine telah terpasang pada komputer santri

# Kompetensi
- Dapat mendeklarasikan variabel dan ragam tipe data
- Dapat menggunakan operator aritmatika, penetapan dan penaik/penurun
- Dapat menggunakan operator perbandingan, dan logika
- Dapat memahami urutan pengutamaan operator

# Materi

## Berkas PHP
Berkas PHP dinamai dengan akhiran ektensi _.php_, di dalam berkas dapat ditulis apapun, misal campur dengan kode HTML. Blok kode PHP diawali dengan `<?php` dan diakhiri dengan `?>`. Jika berkas hanya berisi kode PHP,  `?>` tidak perlu ada diakhir kode.

## Deklarasi variabel
Variabel PHP dimulai tanda `$` diikuti nama variabel.
Nama variable yang sahih diawali dengan huruf atau garisbawah diikuti kombinasi huruf, angka, dan atau garisbawah.
Nama variable _case sensitive_.

## Tipe data
PHP memiliki 8 tipe data primitif, 4 diantaranya adalah:
- **Boolean**, merupakan nilai benar (`true`) atau salah (`false`).
- **Integer**, merupakan nilai bilangan tanpa koma desimal, contoh `1` atau `9`.
- **Float**, merupakan nilai bilangan dengan koma desimal, contoh `3,14`.
- **String**, merupakan rangkaian karakter yang dikurung tanda petik atau petikganda, contoh `'kata'` atau `"lema"`.

Variable PHP dapat ditetapkan ulang nilainya dengan tipe data yang berlainan dan tipe data satu dapat diubah ke tipe data lainnya, ini disebut **type juggling**.

## Operator Aritmatika
...

# Meta
Kata kunci:
- variable
- data types
- type juggling
- 

Tautan:
- [PHP](http://php.net)

# Latihan
1. Tulis kode-kode berikut dalam berkas `tugas-1.php`. Jalankan pada terminal, amati hasilnya, dan terangkan:
```php
// tugas-1.php
// deklarasi variabel
$_some_value = 'abc';
$1number = 12.3;
$som$signs% = '&^%';
$go_2_home = "ok";
$go_2_Home = 'no';
$isThisCamelCase = true;
```
2. Tulis kode-kode berikut dalam berkas `tugas-2.php`. Jalankan pada terminal, amati hasilnya, dan terangkan:
```php
// tugas-2.php
// tipe data
$a = true;
$b = 'false';
$c = 4.5;
$d = 3;
$e = "2";
var_dump($a);
var_dump($b);
var_dump((bool)$b);
var_dump($c);
var_dump((int)$c);
var_dump($d + $e);
var_dump($d . $e);
var_dump($c * $e);
```
3. ...