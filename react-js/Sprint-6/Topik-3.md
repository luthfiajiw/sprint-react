# Kompetensi

- Memahami Box Model, Flexbox dan Grid pada CSS
- Mengerti cara kerja SASS

# Materi

## **CSS Box Model**

Seluruh elemen HTML dapat dibayangkan sebagai box-box. Pada CSS, istilah *Box Model* digunakan ketika berbicara tentang desain dan tata letak tampilan.

Box model css apada intinya adakah sebuah box yang membungkus seluruh elemen HTML yang terdiri dari: margin, border, padding dan konten itu sendiri. Gambar di bawah mengilustrasikan apa itu **Box Model**.

![box model](css-box-model.png)
_CSS Box Model_

Penjelasan pada beberapa bagian:

- **Content** = Isi dari box tersebut, bisa berupa teks atau gambar.
- **Padding** = Membebaskan ruang di sekitar konten. 
- **Border** = Sebiah garis batas yang mengelilingi padding dan konten.
- **Margin** = Mengosongkan area di luar border

Box model mengizinkan kita untuk bisa menambahkan garis di sekitar elemen dan untuk memberikan ruang kosong di antara elemen-elemen.

```css
div {
  width: 300px;
  border: 25px solid green;
  padding: 25px;
  margin: 25px;
}
```

### **Lebar(Width) dan Tinggi(Height) Sebuah Elemen**

Untuk mengatur lebar dan tinggi sebuah elemen dengan benar di semua browser, maka kita perlu tahu cara kerja  box model.

* Ketika kamu mengatur lebar dan tinggi dari sebuah elemen dengan css, kamu perlu mengatur properti lebar dan tinggi di **content area**. Untuk menghitung _full size_ dari sebuah elemen, kamu harus menambahkan padding, border dan margin.

Asumsikan kita ingin men-*style* elemen `<div>` supaya memilik total lebar sebesar 350px.

```css
div {
  width: 320px;
  padding: 10px;
  border: 5px solid gray;
  margin: 0;
}
```

Ini dia perhitungannya:
```
320px (width)
+ 20px (left + right padding)
+ 10px (left + right border)
+ 0px (left + right margin)
= 350px
``` 

## **CSS Flexbox**

Flexible Box Module, biasa disebut sebagai _flexbox_ telah dirancang sebagai model tata ruang satu dimensi dan juga sebagai method yang dapat memberikan pendistribusian ruang antar item pada satu interface dan kemampuan penyelarasan yang kuat.

Ketika kita mengatakan _flexbox_ sebagai satu dimensi, kita menggambarkan fakta bahwa flexbox berhubungan dengan tatak letak dalam satu dimensi pada satu waktu — baik sebagai baris atau sebagai kolom. Hal ini dapat dikontraskan denga model dua dimensi dengan CSS Grid, yang mengontrol kolom dan baris bersama.

Sebelum ada Flexbox, sudah ada 4 tipe tata letak tampilan:
- Block, untuk bagian-bagian di dalam halaman web
- Inline, untuk teks
- Table, untuk tabel data dua dimensi
- Positioned, posisi eksplisit suatu elemen

### **Flexbox Elements**

Untuk memulai menggunakan Flexbox model, kamu harus mendefinisikan sebuah flex container.

![flexbox container](flexbox-1.svg)

Gambar di atas menunjukkan sebuah flex container (area berwarna hijau) dengan 3 flex item.

html:
```html
<div class="flex-container">
  <div>1</div>
  <div>2</div>
  <div>3</div>
</div> 
```

css:
```css
.flex-container {
  display: flex;
  background-color: DodgerBlue;
}

.flex-container div {
  background-color: #f1f1f1;
  margin: 10px;
  padding: 20px;
  font-size: 30px;
}
```

### **Parent Element (Container)**

Flex Container akan menjadi flexible dengan mengatur properti `display` menjadi flex:

```css
.flex-container {
  display: flex;
}
```

### **Properti `flex-direction`**

Properti `flex-direction` mendefinisikan ke arah mana container ingin menumpuk flex item.

![flex direction](flexbox-2.svg)

Value `column` menumpukkan flex item secara vertical dari atas ke bawah

```css
.flex-container {
  display: flex;
  flex-direction: column;
}
```

Value `column-reverse` menumpukkan flex item secara vertical tetapi dari bawah ke atas.

```css
.flex-container {
  display: flex;
  flex-direction: column-reverse;
}
```

Value `row` menumpukkan flex item secara horizontal dari kiri ke kanan.

```css
.flex-container {
  display: flex;
  flex-direction: row;
}
```

Value `row-reverse` menumpukkan flex item secara horizontal tetapi dari kanan ke kiri.

```css
.flex-container {
  display: flex;
  flex-direction: row-reverse;
}
```

### **Property `flex-wrap`**
Properti flex-wrap menentukan apakah flex item harus dibungkus atau tidak. Contoh di bawah ini memiliki 12 flex item

![flex-wrap](flexbox-3.svg)

Value `wrap` menentukan bahwa flex item akan membungkus jika perlu:

```css
.flex-container {
  display: flex;
  flex-wrap: wrap;
}
```

Value `nowrap` menentukan ahwa flex item tdak akan membungkus (ini _default_ nya)

```css
.flex-container {
  display: flex;
  flex-wrap: nowrap;
}
```

Value `wrap-reverse` menentukan bahwa flex item akan membungkus jika perlu, dalam urutan yang terbalik.

```css
.flex-container {
  display: flex;
  flex-wrap: wrap-reverse;
}
```

### **Properti `justify-content`**

Properti `justify-content` digunakan untuk meluruskan flex item:

![justify-content](flexbox-4.svg)

Value `center` untuk meluruskan flex item ke tengah-tengah container

```css
.flex-container {
  display: flex;
  justify-content: center;
}
```

Value `flex-start` untuk meluruskan flex item ke bagian awal container (ini _default_)

```css
.flex-container {
  display: flex;
  justify-content: flex-start;
}
```

Value `flex-end` untuk meluruskan flex item ke bagian akhir container

```css
.flex-container {
  display: flex;
  justify-content: flex-end;
}
```

Value `space-around` untuk menampilkan flex item dengan spasi di sebelum, diantara dan setelah garis.

```css
.flex-container {
  display: flex;
  justify-content: space-around;
}
```

Value `space-between` untuk menampilkan flex item dengan spasi diantara garis

```css
.flex-container {
  display: flex;
  justify-content: space-between;
}
```
---

## **CSS Grid**

![css grid](css-grid-framework.png)

Modul CSS Grid Layout menawarkan sistem layout berbasis grid/kisi, dengan baris dan kolom, membuatnya lebih mudah untuk mendesain halaman web tanpa harus menggunakan `float` dan positioning.

### **Grid Elements**

sebuah grid layout terdiri dari elemen induk/parent element, dengan satu atau lebih elemen anak/child element.

html:
```html
 <div class="grid-container">
  <div class="grid-item">1</div>
  <div class="grid-item">2</div>
  <div class="grid-item">3</div>
  <div class="grid-item">4</div>
  <div class="grid-item">5</div>
  <div class="grid-item">6</div>
  <div class="grid-item">7</div>
  <div class="grid-item">8</div>
  <div class="grid-item">9</div>
</div> 
```

css:
```css
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
  background-color: #2196F3;
  padding: 10px;
}
.grid-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 0, 0.8);
  padding: 20px;
  font-size: 30px;
  text-align: center;
}
```

![grid 1](grid-1.svg)

### **Properti Display**

Sebuah elemen HTML menjadi sebuah container grid dengan mengatur properti `display` ke _grid_ atau _inline-grid_.

```css
.grid-container {
  display: grid;
}
```

```css
.grid-container {
  display: inline-grid;
}
```

Semua elemen anak/children element secara automatis menjadigrid item.

### **Grid Columns**

Garis vertical dari grid item disebut kolom atau _column_.

![grid column](grid-2.svg)

### **Grid Rows**

Garis horizontal dari grid item disebut baris atau _row_

![grid row](grid-3.svg)

### **Grid Gaps**

Ruang kosong diantara masing-masing kolom/row disebuat _gap_

![grip gap](grid-4.svg)

Kamu bisa menyesuaikan ukuran gap engan menggunakan salah satu dari properti berikut.

- grid-column-gap
- grid-row-gap
- grid-gap

Properti `grid-column-gap` mengatur gap/celah diantara kolom. 

```css
.grid-container {
  display: grid;
  grid-column-gap: 50px;
}
```

Properti `grid-row-gap` mengatur gap/celah diantara baris.

```css
 .grid-container {
  display: grid;
  grid-row-gap: 50px;
}
```

Properti `grid-gap` adalah shorthand dari properti `grid-column-gap` dan `grid-row-gap`.

```css
.grid-container {
  display: grid;
  grid-gap: 50px 100px;
}
```

Properti `grid-gap` juga bisa digunakan untuk mengatur row gap dan column gap dalam satu value.

```css
.grid-container {
  display: grid;
  grid-gap: 50px;
}
```

### **Grid Lines**

Garis antara kolom disebut _column line_.
Ganti antara baris disebut _row line_.

![grid line](grid-5.svg)

Lihat nomer garis ketika meletakkan grid item di dalam grid container.

Contoh:
Meletakkan grid item pada garis kolom 1 dan membiarkannya berakhir pada garis kolom 3.

```css
.item1 {
  grid-column-start: 1;
  grid-column-end: 3;
}
```

Contoh:
Meletakkan grid item pada garis baris 1 dan membairkannya berakhir pada garis baris 3.

```css
.item1 {
  grid-row-start: 1;
  grid-row-end: 3;
}
```
----

## **SASS: Super Power CSS**

### **Preprocessing**

CSS sendiri sebenarnya sudah cukup menyenangkan, tapi stylesheets nya semakin lama semakin besar, semaikn kompleks, dan sulit untuk dipelihara. Disini lah _processor_ dapat membantu. Sass membuatmu bisa menggunakan fitur-fitur yang tidak ada pada css seperti variabel, nesting, mixins, inheritance dan banyak hal bagus lainnya.

Ketika kamu mulai bermain dengan Sass, maka file Sass yang sudah kamu proses sebelumnya akan diambil dan disimpan sebagai file CSS normal yang dapat kamu gunakan pada situs web kamu.

Cara untuk mewujudkan hal itu ada pada terminal. Ketika Sass sudah terinstall, kamu bisa meng-_compile_ Sass milikmu ke bentuk CSS menggunakan perintah `sass`. Kamu perlu memberitahu kepada Sass file mana yang kamu ingin ubah ke bentuk CSS, dan dimana tempat untuk menaruh file CSS nya.

Kamu juga bisa mengamati setiap individu file atau folder menggunakan tanda `--watch`. Tanda tersebut memberitahu Sass untuk mengamati sumber filemu jika terdapat perubahan dan meng-_compile_ ulang setiap kali kamu menyimpan file Sass milikmu.

```bash
sass --watch input.scss output.css
```

Kamu bisa mengamati dan menghasilkan output menggunakan jalur folder yang menuju input dan output filenya, dan memisahkan kedua file tersebut dengan tanda titik dua (:), contoh:

```bash
sass --watch app/sass:public/stylesheets
```

Sass akan mengamati perubahan semua file yang ada pada folder `app/sass` dan meng-_compile_ CSS ke folder `public/stylesheets`.

## **Variabel**

Pikirkan bahwa variabel sebagai cara untuk menyimpan informasi yang ingin kamu gunakan kembali di seluruh stylesheetmu. Kamu dapat menyimpan hal-hal seperti warna, tumpukan font, atau value CSS apapun yang akan kamu gunakan kembali. Sass menggunakan simbol `$` untuk membuat sesuatu menjadi variabel, contoh:

```scss
$font-stack:    Helvetica, sans-serif;
$primary-color: #333;

body {
  font: 100% $font-stack;
  color: $primary-color;
}

```

Ketika Sass diproses, maka varibel yang kita definisikan sebagai `$font-stack` dan `primary-color` akan diambil dan dihasilkanlah CSS normal dengan nilai variabel diletakkan pada file CSS nya.

## **Nesting**

Ketika menulis HTML kamu mungkin memperhatikan bahwa HTML memiliki nesting yang jelas dan visual hierarki, sedangkan CSS tidak.

Sass dapat membuat CSS Selectormu menjadi bersarang dengan cara yang mengikuti hierarki visual yang sama dari HTML kamu.

Berhati-hatilah, bahwa aturan yang terlalu bersarang akan menghasilkan CSS yang terlalu berkualitas yang terbukti sulit untuk dipelihara dan umumnya dianggap sebagai praktik yang buruk.

contoh:

scss

```scss
nav {
  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  li { display: inline-block; }

  a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
}

```

css

```css
nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
nav li {
  display: inline-block;
}
nav a {
  display: block;
  padding: 6px 12px;
  text-decoration: none;
}

```

Kamu akan memperhatikan bahwa `ul`, `li`, dan `a` bersarang pada selektor `nav`. Ini adalah cara yang baik untuk mengatur CSS mu menjadi lebih mudah dibaca.

## **Partial**

Kamu dapat membuat bagian-bagian file Sass yang berisi potongan kecil CSS yang bisa kamu sertakan dalam file Sass lainnya.

Ini merupakan cara yang bagus untuk memodularisasi CSS mu dan membantu untuk lebih mudah dipelihara. Partial adalah file Sass sederhana yang memiliki underscore ( _ ) sebagai nama depan. Seperti `_partial.scss`. Tanda underscore membuat Sass mengetahui bahwa file tersebut hanyalah sebuah file partial dan tidak akan dihasilkan ke bentuk CSS. Partial Sass digunakan dengan perintah `@import`.

## **Import**

CSS memiliki pilihan **import** yang bisa membuat CSS mu menjadi lebih kecil dan lebih mudah  dipelihara. 

Anggap saja kita memiliki sepasang file Sass, `_reset.scss` dan `base.scss`. Kita mau mengimpor `_reset.scss` ke `base.scss`:

```scss
// _reset.sass

html,
body,
ul,
ol
  margin:  0
  padding: 0

```

```scss
// base.scss

@import reset

body
  font: 100% Helvetica, sans-serif
  background-color: #efefef
```

Perhatikan bahwa kita menggunakan `@import 'reset';` di dalam file `base.scss`. Ketika kamu mengimpor sebuah file kamu tidak perlu mengikutkan ekstensi `.scss`. Sass itu pintar dan akan mencari tahu sendiri untuk kamu.

## **Mixins**

Beberapa hal yang ada pada CSS sedikit membosankakn untuk ditulis, khususnya dengan deklarasi CSS3 yang ingin kamu gunakan kembali di seluruh situsmu. Kamu bahkan dapat memberikan value untuk membuat mixin kamu lebih fleksibel. Penggunaan mixin yang baik adalah untuk vendor prefix, contoh:

```scss
@mixin transform($property) {
  -webkit-transform: $property;
  -ms-transform: $property;
  transform: $property;
}

.box { @include transform(rotate(30deg)); }

```

Untuk membuat _mixin_ kamu perlu menggunakan perintah `@mixin` dan memberikannya nama. kita telah memberikan nama mixin kita dengan nama `transform`. Kita juga telah menggunakan variabel `$property` di dalam tanda kurung () jadi kita bisa melewati `transform` apapun yang kita mau. Setelah `@mixin` dibuat, kita bisa menggunakannya sebagai deklarasi CSS yang dimulai dengan `@include` kemudian diikuti dengan nama mixin-nya.

## **Inheritance**

Ini adalah fitur yang paling berguna milik Sass. Menggunakan `@extend` membuatmu bisa membagikan beberapa set properti CSS dari satu selektor ke yang lainnya.

Pada contoh kali ini kita akan membuat serangkaian pesan sederhana untuk error, warning dan sukses menggunakan fitur lain yang sejalan dengan kelas extended, kelas placeholder. Kelas placeholder adalah jenis kelas khusus yang hanya mencetak saat di-_extend_ dan dapat membantu menjaga CSS yang dikompilasi rapi dan bersih.

```scss
/* CSS ini akan dicetak karena %message-shared di-extend*/
%message-shared {
  border: 1px solid #ccc;
  padding: 10px;
  color: #333;
}

// CSS ini tidak akan dicetak karena %equal-heights tidak pernah di-extend
%equal-heights {
  display: flex;
  flex-wrap: wrap;
}

.message {
  @extend %message-shared;
}

.success {
  @extend %message-shared;
  border-color: green;
}

.error {
  @extend %message-shared;
  border-color: red;
}

.warning {
  @extend %message-shared;
  border-color: yellow;
}

```

Kode di atas menerangkan bahwa `.message`, `.success`, `.error` & `.warning` untuk berperilaku seperti `%message-shared`. Itu berarti dimanapun `%message-shared` berada maka `.message`, `.success`, `.error` & `.warning` juga. Keajaiban terjadi pada CSS yang dihasilkan, di mana pada masing-masing kelas tersebut akan mendapatkan properti CSS yang sama dengan `%message-shared`. Ini membantu Anda menghindari keharusan menulis banyak nama kelas pada elemen HTML.

Catat bahwa CSS pada `%equal-heights` tidak di-generated karena `%equal-heights` tidak pernah di-extend.

## **Operators**

Melakukan perhitungan di CSS sungguh sangat membantu. Sass memiliki beberapa standar perhitungan matematika seperti `+`, `-`, `*`, `/` dan `%`. Pada contoh dibawah kita akan melakukan perhitungan sederhana unutk menghitung lebar milik `aside` dan `article`.

```scss
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 600px / 960px * 100%;
}

aside[role="complementary"] {
  float: right;
  width: 300px / 960px * 100%;
}

```

Kita telah membuat fluid grid yang sangat sederhana berbasis 960px. Operasi pada Sass membiarkan kita untuk melakukan sesuatu seperti mengambil value pixel dan mengkonversinya ke dalam persentasi tanpa banyak kesulitan.

# Meta
tautan :
- [CSS Flexbox](https://www.w3schools.com/css/css3_flexbox.asp)
- [CSS Grid](https://www.w3schools.com/css/css_grid.asp)
- [SASS: Super Power CSS](https://sass-lang.com/guide)

# Latihan

Buat tampilan berikut menggunakan SASS.

![soal grid](grid-soal.gif)